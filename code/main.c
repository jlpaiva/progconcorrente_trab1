/**
 * Jonatas Lopes de Paiva
 * Marcos Henrique Alves Sandim
 *
 * main.c
 */

#include "solver.h"
#include "file_handler.h"
#include <time.h>

/**
 * Parametros esperados
 * Nome do arquivo de entrada
 * Nome do arquivo de saida
 * Numero de execucoes do algoritmo
 * Numero de threads para o programa (so necessario para a execucao paralela)
 */
int main(int argc, char const *argv[])
{
	
	uint J_ORDER, J_ROW_TEST, J_ITE_MAX;
	
	double J_ERROR, value;

	double **A;
	double *b;

	uint numIter, numThreads, numExecucoes;

	// Verifica algumas entradas
	if(argc < 5) 
	{
		numThreads = 0;
	}
	else 
	{
		numThreads = atoi(argv[4]);
	}

	if(argc < 4)
	{
		fprintf(stderr, "Error: Invalid number of parameters\n");
		return -1;
	}

	numExecucoes = atoi(argv[3]);

	// Le o arquivo de entrada
	readfile(argv[1], &J_ORDER, &J_ROW_TEST, &J_ERROR, &J_ITE_MAX, &A, &b);

	uint i, j;

	// Cria o arquivo de saida
	FILE *fp = fopen(argv[2], "w");

	// Cria vetores para calculos dos desvios dos tempos de execucao
	// e dos resultados obtidos
	double elapsedTimes[numExecucoes];
	double sumTimes = 0.0;

	double results[numExecucoes];
	double sumResults = 0.0;

	for(i = 0; i < numExecucoes; i++)
	{

		struct timespec start, finish;
		double elapsed;

		clock_gettime(CLOCK_MONOTONIC, &start);

		// Executa o algoritmo em si
		double* x = solve(A, b, J_ORDER, J_ERROR, J_ITE_MAX, numThreads, &numIter);

		clock_gettime(CLOCK_MONOTONIC, &finish);

		// Calcula o tempo de execucao do algoritmo
		elapsedTimes[i] = (finish.tv_sec - start.tv_sec);
		elapsedTimes[i] += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;

		sumTimes += elapsedTimes[i];

		// Imprime apenas no arquivo
		fprintf(fp, "---------------------- Execution %d ----------------------\n", (i+1));
		
		if (x)
		{
			// Faz a validacao do resultado com a linha indicada no arquivo de entrada
			value = 0.0;
			for (j = 0; j < J_ORDER; j++)
			{
				value += A[J_ROW_TEST][j] * x[j];
			}
			results[i] = value;
			sumResults += value;

			// Imprime as saidas no arquivo
			fprintf(fp, "Elapsed Time: %lf\n", elapsedTimes[i]);
			fprintf(fp, "Iterations: %d\n", numIter);
			fprintf(fp, "RowTest: %d => [%f] =? %f\n", J_ROW_TEST, value, b[J_ROW_TEST]);
			
		}
		else
		{
			fprintf(fp, "Error: Empty value for x\n");
		}

		fprintf(fp, "---------------------------------------------------------\n");

		// libera x
		free(x);

	}

	// Calcula as medias e desvios padrao dos tempos de execucao
	// e resultados obtidos para o X (calculados na linha J_ROW_TEST)
	double stdevTime = 0.0, stdevResults = 0.0;
	double avgTime = sumTimes/((double)numExecucoes);
	double avgResults = sumResults/((double)numExecucoes);

	for(i = 0; i < numExecucoes; i++)
	{
		stdevResults += pow(results[i] - avgResults, 2);
		stdevTime += pow(elapsedTimes[i] - avgTime, 2);
	}

	stdevResults = sqrt(stdevResults / ((double)numExecucoes-1));
	stdevTime = sqrt(stdevTime / ((double)numExecucoes-1));

	// Imprime todas essas estatisticas no arquivo
	fprintf(fp, "\nTime statistics:\n");
	fprintf(fp, "Average: %lf seconds.\n", avgTime);
	fprintf(fp, "Standard deviation: %lf .\n", stdevTime);

	fprintf(fp, "\nResults statistics:\n");
	fprintf(fp, "RowTest: %d => (Average)[%lf] =? %f\n", J_ROW_TEST, avgResults, b[J_ROW_TEST]);
	fprintf(fp, "Standard deviation: %lf \n", stdevResults);

	fclose(fp);

	// Na tela exibe apenas as medias
	printf("Average time: %lf seconds.\n", avgTime);
	printf("Average result: [%lf] =? %f\n", avgResults, b[J_ROW_TEST]);
	
	// Libera a memoria
	free(b);
	for(j = 0; j < J_ORDER; j++)
	{
		free(A[j]);
	}
	free(A);

	return 0;
}
