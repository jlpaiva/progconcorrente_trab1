/**
 * Jonatas Lopes de Paiva
 * Marcos Henrique Alves Sandim
 *
 * Define a funcao para execucao do algoritmo Jacobi-Richardson
 */

#include <stdlib.h>
#include <math.h>

/**
 * Entradas:
 * 			- matA - matriz de entradas A
 *			- matB - vetor de entradas b
 * 			- dimension - dimensao de A e b
 *			- maxError - O erro maximo para estabelecer o criterio de parada do algoritmo
 *			- maxIter - O numero maximo de iteracoes a ser executado pelo algoritmo, caso o criterio de parada
 *						nao seja atingido antes
 *			- numThreads - O numero de threads nos quais este algoritmo ira ser executado. Caso a implementacao 
 *							seja sequencial este parametro não necessita ser utilizado
 *			- numIter - Variavel que ira retornar o numero de iteracoes que o algoritmo precisou para atingir
 * 						um dos criterios de parada.
 */
double* solve(double** matA, double* matB, uint dimension, double maxError, uint maxIter, uint numThreads, uint *numIter);
