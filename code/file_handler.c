/**
 * Jonatas Lopes de Paiva
 * Marcos Henrique Alves Sandim
 *
 * Implementa as definicoes de file_handler.h
 * Realiza a leitura do arquivo com as entradas para o Jacobi-Richardson
 */

#include "file_handler.h"

void readfile(const char *fileName, uint *J_ORDER, uint *J_ROW_TEST, double *J_ERROR, uint *J_ITE_MAX, double ***A, double **b) {

	FILE *fp = fopen(fileName, "r");

	fscanf(fp, "%d", J_ORDER);
	fscanf(fp, "%d", J_ROW_TEST);
	fscanf(fp, "%lf", J_ERROR);
	fscanf(fp, "%d", J_ITE_MAX);

	// Aloca memoria e le a matriz de entrada (A)
	*A = (double**)malloc((*J_ORDER) * sizeof (double*));
	uint i, j;
	double value;

	for(i = 0; i < (*J_ORDER); i++)
	{
		(*A)[i] = (double*) malloc((*J_ORDER) * sizeof(double));

		for(j = 0; j < (*J_ORDER); j++)
		{
			fscanf(fp, "%lf ", &((*A)[i][j]));
		}
	}

	// Aloca memoria e le o vetor b
	*b = (double*) malloc((*J_ORDER) * sizeof(double));

	for(i = 0; i < (*J_ORDER); i++)
	{
		fscanf(fp, "%lf", &((*b)[i]));
	}

	fclose(fp);

}
