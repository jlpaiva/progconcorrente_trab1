/**
 * Jonatas Lopes de Paiva
 * Marcos Henrique Alves Sandim
 *
 * Implementa as definicoes de solver.h de maneira sequencial
 */

#include <omp.h>
#include <stdio.h>
#include "solver.h"

/**
 * Nesta versao a variavel numThreads nao e utilizada
 */
double* solve(double** matA, double* matB, uint dimension, double maxError, uint maxIter, uint numThreads, uint *numIter)
{
	// Aloca e cria as versoes modificadas (L R) de A e b

	double** matAMod = (double**)malloc(dimension * sizeof (double*));
	double* matBMod = (double*) malloc(dimension * sizeof(double));
	double* x = (double*) malloc(dimension * sizeof(double));

	int i, j;

	omp_set_num_threads(4);

	for(i = 0; i < dimension; i++)
	{
		matAMod[i] = (double*) malloc(dimension * sizeof(double));
		matBMod[i] = matB[i] / matA[i][i];
		x[i] = matBMod[i];

		double sum = 0.0;
		for(j = 0; j < dimension; j++)
		{
			if(i == j)
			{
				matAMod[i][j] = 0;
			}
			else
			{
				matAMod[i][j] = matA[i][j]/matA[i][i];
				sum += matAMod[i][j];
			}
		}

		// Algoritmo nao ira convergir
		if(sum >= 1.0)
		{
			free(x);
			free(matBMod);

			for(j = 0; j <= i; j++)
			{
				free(matAMod[j]);
			}
			free(matAMod);

			return 0;
		}

	}

	// Executa o passo iterativo do metodo.
	*numIter = 0;
	double erro;
	double* xTemp = (double*) malloc(dimension * sizeof(double));
	do
	{
		// Percorre todas as linhas da matriz calculando os novos valores de X
		double maxDiff = 0.0;
		double maxValue = 0.0;
		#pragma omp parallel for //shared(maxDiff, maxValue) private(matAMod, matBMod)
		for(i = 0; i < dimension; i++)
		{
			double sum = 0.0;

			for(j = 0; j < dimension; j++)
			{
				sum += matAMod[i][j]*x[j];
			}

			double tmp = matBMod[i] - sum;

			double diff = fabs(tmp - x[i]);

			// Acha a maior diferenca (x_k+1 - x_k)
			if(diff > maxDiff)
			{
				maxDiff = diff;
			}

			xTemp[i] = tmp;

			// Acha o maior valor para x
			tmp = fabs(tmp);
			if(tmp > maxValue)
			{
				maxValue = tmp;
			}
		}

		// Atualiza x
		for(i = 0; i < dimension; i++)
		{
			x[i] = xTemp[i];
		}

		erro = maxDiff/maxValue;

		(*numIter)++;
	}
	while((*numIter) < maxIter && erro > maxError);

	// Libera a memoria alocada
	free(matBMod);
	for(i = 0; i < dimension; i++)
	{
		free(matAMod[i]);
	}
	free(matAMod);
	free(xTemp);


	return x;
}
