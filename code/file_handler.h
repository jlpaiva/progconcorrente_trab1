/**
 * Jonatas Lopes de Paiva
 * Marcos Henrique Alves Sandim
 *
 * Define a funcao de leitura de arquivos
 */

#include <stdio.h>
#include <stdlib.h>

void readfile(const char *fileName, uint *J_ORDER, uint *J_ROW_TEST, double *J_ERROR, uint *J_ITE_MAX, double ***A, double **b);