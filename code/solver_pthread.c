/**
 * Jonatas Lopes de Paiva
 * Marcos Henrique Alves Sandim
 *
 * Implementa as definicoes de solver.h de maneira paralela utilizando pthreads
 */

#include "solver.h"
#include <pthread.h>

/**
 * Define uma estrutura a ser utilizada como entrada e saida de valores para cada thread
 */
typedef struct thread_worker_data{
	int  thread_id;
	uint  start, end;
	uint dimension;
	double **matA;
	double *matB;
	double *x;
	double *xTemp;
	double maxDiff, maxValue;
} thread_worker_data;

/**
 * Implementacao das funcionalidades de uma thread
 */
void *solverWorker(void *vargp)
{
	thread_worker_data *a = (thread_worker_data *) vargp;
	
	a->maxDiff = 0.0;
	a->maxValue = 0.0;

	double tmp, diff, sum;

	int i, j;

	// Realiza a iteracao do Jacobi-Richardson apenas dentro do intervalo 
	// no qual essa thread e responsavel
	for(i = a->start; i < a->end; i++)
	{
		sum = 0.0;

		for(j = 0; j < a->dimension; j++)
		{
			sum += a->matA[i][j]*a->x[j];
		}

		tmp = a->matB[i] - sum;

		diff = fabs(tmp - a->x[i]);

		// Encontra a maior diferenca
		if(diff > a->maxDiff)
		{
			a->maxDiff = diff;
		}

		// Atualiza o X temporario
		a->xTemp[i] = tmp;

		// Encontra o maior valor
		tmp = fabs(tmp);
		if(tmp > a->maxValue)
		{
			a->maxValue = tmp;
		}
	}

	// encerra a thread
	pthread_exit((void *)NULL);
}

/**
 * Implementa a definicao do arquivo header. O parametro numThreads e utilizado
 */
double* solve(double** matA, double* matB, uint dimension, double maxError, uint maxIter, uint numThreads, uint *numIter)
{
	// Aloca e cria as versoes modificadas (L R) de A e b
	double** matAMod = (double**)malloc(dimension * sizeof (double*));
	double* matBMod = (double*) malloc(dimension * sizeof(double));
	double* x = (double*) malloc(dimension * sizeof(double));

	int i, j;

	for(i = 0; i < dimension; i++)
	{
		matAMod[i] = (double*) malloc(dimension * sizeof(double));
		matBMod[i] = matB[i] / matA[i][i];
		x[i] = matBMod[i];

		double sum = 0.0;
		for(j = 0; j < dimension; j++)
		{
			if(i == j)
			{
				matAMod[i][j] = 0;
			}
			else
			{
				matAMod[i][j] = matA[i][j]/matA[i][i];
				sum += matAMod[i][j];
			}
		}

		// Algoritmo nao ira convergir
		if(sum >= 1.0)
		{
			free(x);
			free(matBMod);

			for(j = 0; j <= i; j++)
			{
				free(matAMod[j]);
			}
			free(matAMod);
			return 0; 
		}
	}

	*numIter = 0;
	double erro;
	double* xTemp = (double*) malloc(dimension * sizeof(double));

	// Calcula a faixa de valores sobre a qual cada thread
	// sera responsavel
	uint sliceSize = (uint) (((double)dimension)/((double)numThreads));

	// Cria os vetores com as threads e seus dados
	pthread_t workers[numThreads];
	thread_worker_data workersData[numThreads];

	// Define os dados que nao serao mudados e serao utilizados
	// pela thread em cada iteracao
	for(i=0; i < numThreads; i++)
	{
		workersData[i].thread_id = i;
		workersData[i].matA = matAMod;
		workersData[i].matB = matBMod;
		workersData[i].start = i*sliceSize;
		workersData[i].end = (i+1) * sliceSize;
		workersData[i].dimension = dimension;
	}

	// A ultima thread e ajustada para levar ate o ultimo elemento, caso a divisao nao seja exata
	workersData[numThreads-1].end = dimension;

	do
	{
		// Atualiza os valores de x e xTemp para cada thread e as inicia
		for(i=0; i < numThreads; i++)
		{
			workersData[i].x = x;
			workersData[i].xTemp = xTemp;
			pthread_create(&(workers[i]), NULL, solverWorker, (void *)&(workersData[i]));
		}

		// Aguarda todas as threads se encerrarem
		for(i=0; i<numThreads; i++)
		{
			pthread_join(workers[i], NULL);
		}

		// Calcula os maximos para a diferenca e o X calculado
		double maxDiff = 0.0;
		double maxValue = 0.0;

		for(i=0; i<numThreads; i++)
		{
			if (workersData[i].maxDiff > maxDiff)
			{
				maxDiff = workersData[i].maxDiff;
			}

			if (workersData[i].maxValue > maxValue)
			{
				maxValue = workersData[i].maxValue;
			}
		}

		// Atualiza X
		for(i = 0; i < dimension; i++)
		{
			x[i] = xTemp[i];
		}

		erro = maxDiff/maxValue;

		(*numIter)++;
	}
	while((*numIter) < maxIter && erro > maxError);

	// Desaloca a memoria
	free(matBMod);
	for(i = 0; i < dimension; i++)
	{
		free(matAMod[i]);
	}
	free(matAMod);
	free(xTemp);

	return x;
}
